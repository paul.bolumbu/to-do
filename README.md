Pour commencer , j'ai créé un fichier index qui dirige vers différentes pages.

Ensuite, dans ce fichier nous avons plusieurs choix, se connecter et/ou s'inscrire.

L'inscription demande plusieurs informations, le mail, le mot de passe, la confirmation du mail et mdp et un pseudo.

Une fois l'inscription faite, les données sont envoyé dans la base de données.

La base de donnés se trouve dans un fichier sql du nom de "databases" . Dedans, nous pouvons trouver le nom de la database, elle se nomme "Todo".

Il y a deux tables qui ont étaient créé. "User" pour stocker les informations de l'utilisateur lorsqu'il s'inscrit et 
"Tasks" pour stocker les tâches de la todo list.

Après l'inscription, qui est dans le fichier "inscription.php", on se trouve dans la page "connexion.php"
Dans la page connexion faut juste taper son email et son mdp.

Par la suite, on se trouve dans la page du profil.
Cette page, "Profil.php" possède plusieurs directions. Nous pouvons nous rendre dans "editionprofil.php" une page qui permet de modifier ses informations comme son mail, nom d'utilisateur ...

Toujours depuis la page profil, nous pouvons nous déconnecter et nous pouvons accéder à notre to do list.

La todo list se trouve un fichier "todo.php".
La todo list permet de rajouter des tâches et d'en supprimer.

Il y a une barre qui permet d'écrire la tâche à faire et une croix qui permet de la supprimer.
Quand on clique sur la croix , la tâche part de la BD.
